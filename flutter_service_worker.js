'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "version.json": "a40b5abba8ea08a01bd409aca6065b8b",
"index.html": "5139a3792024e30cbf6a014bfdf9a457",
"/": "5139a3792024e30cbf6a014bfdf9a457",
"main.dart.js": "760be23a97f5a813e9cf1270bd231115",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "9109a9a5d8c2349d5221460d632d6a17",
"assets/AssetManifest.json": "3bbce797f3252e6102d2d6229ecc85cd",
"assets/NOTICES": "bf12c85bd8e7c90cc99a04c8898bfaf0",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/fluttertoast/assets/toastify.js": "e7006a0a033d834ef9414d48db3be6fc",
"assets/packages/fluttertoast/assets/toastify.css": "a85675050054f179444bc5ad70ffc635",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/assets/images/order/icon_more.png": "b2d4ce3e49ac5312c1f996a240766a3b",
"assets/assets/images/order/icon_order_member.png": "6f07315e5052e0c01e0fd7fe77cc1a31",
"assets/assets/images/order/2.0x/icon_more.png": "e29a8b1c5066dbb08cb578d291d5750b",
"assets/assets/images/order/2.0x/icon_order_member.png": "54c3cda70ad74c0bbc3da478fd3e1675",
"assets/assets/images/order/2.0x/icon_order_goldpay.png": "0a973716cbb95ca8b96aeee584a8fef3",
"assets/assets/images/order/2.0x/icon_order_btnCoin.png": "a2e9e519bb902af14d4e80f8e5559e26",
"assets/assets/images/order/2.0x/icon_order_topbg.png": "3d5d99836af6ac0fb6f0310d255e190a",
"assets/assets/images/order/2.0x/icon_order_btnMember.png": "b33ff0ee8d3018f441a79b699df3cd67",
"assets/assets/images/order/2.0x/icon_order_coin.png": "f34a44ceb81378ef9c726d57151413ac",
"assets/assets/images/order/2.0x/icon_order_btnSel.png": "363ed1e9e6ab83a04485300c7e12f2b4",
"assets/assets/images/order/2.0x/icon_order_blank.png": "e196495755029a8c76aa9701f8e3ae1b",
"assets/assets/images/order/2.0x/icon_order_activity.png": "480854af5eefaf4152355d5d6ab5c8fa",
"assets/assets/images/order/2.0x/icon_service.png": "3b14504ad8bf2bfe49479bf763bc68b4",
"assets/assets/images/order/icon_order_goldpay.png": "4e01825d2f84baa675406490864f5cd3",
"assets/assets/images/order/icon_order_btnCoin.png": "61153d52217ca38013d86ad40454a1d3",
"assets/assets/images/order/icon_order_topbg.png": "e6fb039f25904292139e93753c228b1b",
"assets/assets/images/order/3.0x/icon_more.png": "3a330cf644acc4c62ee9cbf774486502",
"assets/assets/images/order/3.0x/icon_order_member.png": "63cd534809d2cc4eb8a7c2ebf9d99fa2",
"assets/assets/images/order/3.0x/icon_order_goldpay.png": "41da19eb63d83a208bcfc58162270cbd",
"assets/assets/images/order/3.0x/icon_order_btnCoin.png": "d4618021b41ada5572a79ea038fb7d88",
"assets/assets/images/order/3.0x/icon_order_topbg.png": "c84a182e7b511354843f11aabd69a2c0",
"assets/assets/images/order/3.0x/icon_order_btnMember.png": "d113035885d2434e66873a71b2652b62",
"assets/assets/images/order/3.0x/icon_order_coin.png": "73be5300b1f7e0902704f5330c87d8fc",
"assets/assets/images/order/3.0x/icon_order_btnSel.png": "68bb6931888444ddf993612979cc0fd3",
"assets/assets/images/order/3.0x/icon_order_blank.png": "a51d16f723aa863784a7ae4a18bce269",
"assets/assets/images/order/3.0x/icon_order_activity.png": "c0596523946b9b88abb6b8e0c08bc7cc",
"assets/assets/images/order/3.0x/icon_service.png": "7e2864649eb9ccbfdf07ecdd82475dc2",
"assets/assets/images/order/icon_order_btnMember.png": "0b8957abd9ebcf7f109c7e0d6ea3cfb7",
"assets/assets/images/order/icon_order_coin.png": "91e6c9734be00e568e884300bc7ce3c5",
"assets/assets/images/order/icon_order_btnSel.png": "d7fb6d6e317c9337916c7f70f71c61c9",
"assets/assets/images/order/icon_order_blank.png": "cfbd525a445f36d604a46fff541c58a7",
"assets/assets/images/order/icon_order_activity.png": "070b7bb6473cf6a444edd213eb4f1781",
"assets/assets/images/order/icon_service.png": "3be396857137339d919e56df302a6115",
"assets/assets/images/draft/icon_drafts_disSel.png": "89c637087728d25edf9b5afb877eae88",
"assets/assets/images/draft/icon_close.png": "d350784988743624be18502200615095",
"assets/assets/images/draft/ic_home_rainbow.png": "a0c474cd41df61f991c151bd215ef77a",
"assets/assets/images/draft/img_home_publish_camera.png": "efbbf1716d0ccf847a2335f44f02cde6",
"assets/assets/images/draft/2.0x/icon_drafts_disSel.png": "1465beedcf71e7824c9aa312e70737d1",
"assets/assets/images/draft/2.0x/icon_close.png": "2a9766b1fedf204cfab18eba440ad180",
"assets/assets/images/draft/2.0x/ic_home_rainbow.png": "23e39938d0e82b15c43065d5bdd15497",
"assets/assets/images/draft/2.0x/img_home_publish_camera.png": "85698687c50eca2e579bee29e2f23c16",
"assets/assets/images/draft/2.0x/icon_me_draft.png": "689c54874ae0f31761a36f9405554a76",
"assets/assets/images/draft/2.0x/icon_draft_edit.png": "83a77b31d8adf7cb259d6851c30c71ad",
"assets/assets/images/draft/2.0x/icon_publish_saveBG.png": "ae04212d191d44b8908f6208e94136dc",
"assets/assets/images/draft/2.0x/icon_draft_camera.png": "ce9927dcc1c5208e5e199c4d28c75cf6",
"assets/assets/images/draft/2.0x/icon_drafts_sel.png": "3a3735e14c6259eeeea348d81e6cda8f",
"assets/assets/images/draft/2.0x/icon_draft_tipsBg.png": "b2c8586e362d0b6c99314429f51da2cf",
"assets/assets/images/draft/2.0x/icon_draft_reset.png": "565fad4f6f1ef882319584e4afcc63c9",
"assets/assets/images/draft/2.0x/img_home_publish_photo.png": "c77872e9d1b3636657e8cb6edc6ffb27",
"assets/assets/images/draft/3.0x/icon_drafts_disSel.png": "34b33753688aedbfd7eaa9c42a0e83a3",
"assets/assets/images/draft/3.0x/icon_close.png": "e7e1f4a42d2e2bb082a210a8b1342641",
"assets/assets/images/draft/3.0x/ic_home_rainbow.png": "a14a72159728c35de5e3a4a991d940c6",
"assets/assets/images/draft/3.0x/img_home_publish_camera.png": "a409daf865ff60c7a3da457bddbe4404",
"assets/assets/images/draft/3.0x/icon_me_draft.png": "b622e2c173d9e902a61499bfc8240358",
"assets/assets/images/draft/3.0x/icon_draft_edit.png": "92c3a2c94451a939e5ea1cc9687291fe",
"assets/assets/images/draft/3.0x/icon_publish_saveBG.png": "b5998fce1cbc656e3897bd4d2efb7e7f",
"assets/assets/images/draft/3.0x/icon_draft_camera.png": "9786b63dda9e397695e12ede3c96d255",
"assets/assets/images/draft/3.0x/icon_drafts_sel.png": "e4ff76297dac71ab2870b9d3f663b64a",
"assets/assets/images/draft/3.0x/icon_draft_tipsBg.png": "574de85b523891661972cd89368f3e75",
"assets/assets/images/draft/3.0x/icon_draft_reset.png": "832815a05b9af7d83b976cc9d0190106",
"assets/assets/images/draft/3.0x/img_home_publish_photo.png": "d68db6fcc207e18fb90ca130229c9619",
"assets/assets/images/draft/icon_me_draft.png": "9272e0de73fa2409c2db33e3776b1730",
"assets/assets/images/draft/icon_draft_edit.png": "7fed076f8d5258783f1420708b1276cc",
"assets/assets/images/draft/icon_publish_saveBG.png": "e67f5fa98e547415af8b862f26099fda",
"assets/assets/images/draft/icon_draft_camera.png": "505a41f22443fb5f82c4ac4cdd984685",
"assets/assets/images/draft/icon_drafts_sel.png": "a73e7153e3da1c9d3a6381191dcfd09d",
"assets/assets/images/draft/icon_draft_tipsBg.png": "4e2d5c6ae8aae3f0cd1dacd06f481a62",
"assets/assets/images/draft/icon_draft_reset.png": "43f74ea4bd9de0314ce3589f72fdc7df",
"assets/assets/images/draft/img_home_publish_photo.png": "bc0c3afbbfeeffb4d406ff28fe52c756",
"assets/assets/images/home/ic_gov.png": "e153f17478f1bc16b0f7631a12f68363",
"assets/assets/images/home/default_logo.png": "b3a7fada6bbd9d79c509e722c9305f3b",
"assets/assets/images/home/ic_vip.png": "ed537d8063213e5ad03f8f197169d7a9",
"assets/assets/images/home/img_home_buycoinbg.png": "943b7882fe9131b1cf802620d2d0b6b3",
"assets/assets/images/home/home_comment.png": "8c43087b40e0eb5a4c046d64a9418b14",
"assets/assets/images/tabbar/tab_home_sel.png": "340fd6616c53855ea8237e260cb15b29",
"assets/assets/images/tabbar/tab_create.png": "50078f34247d293847d44b2921fdb335",
"assets/assets/images/tabbar/tab_mine.png": "816e1b4a4c43a69db172f1d824b33e59",
"assets/assets/images/tabbar/tab_follow_sel.png": "d564a13de41fd4bbe37d3371379a3b22",
"assets/assets/images/tabbar/tab_home.png": "8bcb0e9314d1a6d5d3caf55652365086",
"assets/assets/images/tabbar/tab_message.png": "5d83529182c78872a7c5c80b5074970f",
"assets/assets/images/tabbar/3.0x/tab_home_sel.png": "340fd6616c53855ea8237e260cb15b29",
"assets/assets/images/tabbar/3.0x/tab_create.png": "50078f34247d293847d44b2921fdb335",
"assets/assets/images/tabbar/3.0x/tab_mine.png": "816e1b4a4c43a69db172f1d824b33e59",
"assets/assets/images/tabbar/3.0x/tab_follow_sel.png": "d564a13de41fd4bbe37d3371379a3b22",
"assets/assets/images/tabbar/3.0x/tab_home.png": "8bcb0e9314d1a6d5d3caf55652365086",
"assets/assets/images/tabbar/3.0x/tab_message.png": "5d83529182c78872a7c5c80b5074970f",
"assets/assets/images/tabbar/3.0x/tab_follow.png": "7ca74e21c1df7a033349f6f533db2abd",
"assets/assets/images/tabbar/3.0x/tab_mine_sel.png": "cac65047b98e8488027622868dd996c7",
"assets/assets/images/tabbar/3.0x/tab_message_sel.png": "849a7d28c70e611145bb026ed081a688",
"assets/assets/images/tabbar/3.0x/tab_create_sel.png": "0ec47d81ef3ea8b6ec0acbd763864dbb",
"assets/assets/images/tabbar/3.0x/tab_hot_sel.png": "d14d00ef86ad383f90bbb764cf9b74d8",
"assets/assets/images/tabbar/3.0x/tab_hot.png": "b4721daf83af3269bbd35cdbb4de3489",
"assets/assets/images/tabbar/tab_follow.png": "7ca74e21c1df7a033349f6f533db2abd",
"assets/assets/images/tabbar/tab_mine_sel.png": "cac65047b98e8488027622868dd996c7",
"assets/assets/images/tabbar/tab_message_sel.png": "849a7d28c70e611145bb026ed081a688",
"assets/assets/images/tabbar/tab_create_sel.png": "0ec47d81ef3ea8b6ec0acbd763864dbb",
"assets/assets/images/tabbar/tab_hot_sel.png": "d14d00ef86ad383f90bbb764cf9b74d8",
"assets/assets/images/tabbar/tab_hot.png": "b4721daf83af3269bbd35cdbb4de3489",
"assets/assets/images/me/me_banner_hot_left.png": "9ed3421745df1b4581382ba6d3258750",
"assets/assets/images/me/edit_nor.png": "3a1d6353445e0a6724455da2f168a56b",
"assets/assets/images/me/ic_me_vip_year.png": "e8cf4e0a32ca0bfdecab11cdf677bb4c",
"assets/assets/images/me/ic_me_goldcoin.png": "cdb81e790180e950bf2ecdf7e88a5a3e",
"assets/assets/images/me/img_me_vip_crown.png": "cd03a87d694ff03a44247ef4109aba12",
"assets/assets/images/me/img_me_coupon_btn.png": "ff13fce8c3ab0188244f981ca830f546",
"assets/assets/images/me/img_wallet_titlebg.png": "cb04958c7f711059f16a748e2106d4c3",
"assets/assets/images/me/2.0x/me_banner_hot_left.png": "c4e6975a223d07a04edd9da0c694925d",
"assets/assets/images/me/2.0x/edit_nor.png": "015d7555415cee04c4b8826bceba0d73",
"assets/assets/images/me/2.0x/ic_me_vip_year.png": "0130ea5cd92eb37f5516f9f762980c0b",
"assets/assets/images/me/2.0x/ic_me_goldcoin.png": "d7e26f9a02aa57e16192e4cc7ce7b62a",
"assets/assets/images/me/2.0x/img_me_vip_crown.png": "e03c11ad3e76a0b7d653cfe197eccbc6",
"assets/assets/images/me/2.0x/img_me_coupon_btn.png": "c0939fdb268e6601a2c217fbf07af96d",
"assets/assets/images/me/2.0x/img_wallet_titlebg.png": "2ea654618b9cb5545a759b9e3e88347b",
"assets/assets/images/me/2.0x/img_me_up.png": "e3c13f43b353ac517da60032bf87d5b2",
"assets/assets/images/me/2.0x/me_vip_card.png": "f6c1a6da894ff79713a83ad5948f2293",
"assets/assets/images/me/2.0x/vip_tips.png": "54a8343a97a214ac196197a303133c1b",
"assets/assets/images/me/2.0x/me_vip_bg.png": "888d16b1a7e61093d70ef555c91c215a",
"assets/assets/images/me/2.0x/vip_member_never.png": "10282e39746f0b8f9717f81770732f07",
"assets/assets/images/me/2.0x/center_set.png": "b413ac3dd5c11425a55ad556717dc11a",
"assets/assets/images/me/2.0x/vip_level_bg.png": "d3896a591c225cf269d1390663aac2b6",
"assets/assets/images/me/2.0x/ic_me_set.png": "9b793b6bc7cb717ffab023d495fbb6bc",
"assets/assets/images/me/2.0x/img_me_vip_select.png": "2a0ede035c63bf56a019e18310d948f8",
"assets/assets/images/me/2.0x/vip_permant_bg_select.png": "b9a528231c0e3763e0a42967b4c5c41e",
"assets/assets/images/me/2.0x/edit_sel1.png": "9e4422405b3f9c23eb4ce425f8102957",
"assets/assets/images/me/2.0x/icon_server.png": "f7cffc25514255d608e65fd65310fe2d",
"assets/assets/images/me/2.0x/vip_top_bg.png": "9968070635af26192aaa30115b663e4a",
"assets/assets/images/me/2.0x/img_me_vip_btn.png": "0ab1bc53197c624dd98a0d451b4540a6",
"assets/assets/images/me/2.0x/ic_more.png": "dab5d744e9558b3cb6fb0b9f637b042a",
"assets/assets/images/me/2.0x/ic_me_edit.png": "8f95ffd5bbdef2b2a9c48b36323f1455",
"assets/assets/images/me/2.0x/vip_member_3.png": "4309af3bb59148a3a451d32e7995442a",
"assets/assets/images/me/2.0x/img_me_wallet_titlebg.png": "ab5446a1385acb47a39bb7efde99e349",
"assets/assets/images/me/2.0x/ic_me_vipno.png": "01c5b8fdaf29954e438fd9d755a53067",
"assets/assets/images/me/2.0x/vip_member_1.png": "e7251d4908ad141936c7ad164c6e25cc",
"assets/assets/images/me/2.0x/vip_bandon_top.png": "9c57917fd911e0c7433709debbb5d527",
"assets/assets/images/me/2.0x/me_banner_coupon_left.png": "53bc6755df8f9f145d0b3971f9ea0c3d",
"assets/assets/images/me/2.0x/me_vip_level.png": "c7aa53d95586197e1d3e83dc6c004441",
"assets/assets/images/me/2.0x/me_banner_coupon_title.png": "37a54ea839a7301d732dfdbb67163786",
"assets/assets/images/me/2.0x/vip_rim.png": "294a932b0c53ee6f9d1c5aa93ac08ab4",
"assets/assets/images/me/2.0x/img_me_vip_choose.png": "41cfe9c9f89b2e54cd75cf14b5285c93",
"assets/assets/images/me/2.0x/vip_year_select.png": "ece718bf25252b42ac67b76544ad0fb6",
"assets/assets/images/me/2.0x/ic_question.png": "7e809d23c0f5d3f4bb9eccefd2680eb3",
"assets/assets/images/me/2.0x/vip_Fmvo.png": "f5b7c0c174ce45415a5c321c0b4f8384",
"assets/assets/images/me/2.0x/me_banner_hot_bg.png": "4194057110b99a2489fc561c558f859c",
"assets/assets/images/me/2.0x/ic_me_vip_quarter.png": "673b52a90f5ac4871fd5a43245f7230a",
"assets/assets/images/me/2.0x/ic_me_vip_forever.png": "8b71ec16db73443b90579a25476a888f",
"assets/assets/images/me/2.0x/img_me_wallet_title.png": "e99b323e0a19e75b1880867944b6223c",
"assets/assets/images/me/2.0x/img_me_topbg.png": "e64c395f50cc6b3ccca073d6e15c568e",
"assets/assets/images/me/2.0x/uper_legalize.png": "778928ce60f1e57f48250dda1c59e93d",
"assets/assets/images/me/2.0x/ic_me_vip_month.png": "169d1d6b1be2f3f48eddd1192cee075e",
"assets/assets/images/me/2.0x/btn_close.png": "030bfdcbc447c8dd95c638aa2f67bb63",
"assets/assets/images/me/2.0x/img_me_vip_title.png": "392c1f03cd54339d77535dd08c2173a0",
"assets/assets/images/me/2.0x/vip_member_365.png": "bdfa91c8031c7491e886c4f12abdcc1b",
"assets/assets/images/me/2.0x/icon_vip_item_tips.png": "8969616bf99d68263d1164c232f7fce3",
"assets/assets/images/me/2.0x/me_banner_hot_title.png": "c1f3b89ae1f05f91f626d147309ad399",
"assets/assets/images/me/2.0x/me_banner_coupon_bg.png": "325672e02ecccfc98c295ff1aeb61959",
"assets/assets/images/me/2.0x/uper_tips.png": "32ef95af5c4868c3c6cf6603a9424eb1",
"assets/assets/images/me/2.0x/img_me_wallet_btn.png": "2988bcab9b37757169f98193270fc23c",
"assets/assets/images/me/img_me_up.png": "e82cf48747fabc3f61d32cd4c8b95672",
"assets/assets/images/me/me_vip_card.png": "65e9594da71ab03cadc15d013e5e3f7f",
"assets/assets/images/me/vip_tips.png": "1fb39b28e257ad027f782dd3831097c5",
"assets/assets/images/me/me_vip_bg.png": "3e985d78350b68dc9fc8ad7cb2ef1fe5",
"assets/assets/images/me/vip_member_never.png": "e04847f899f71a4365412d718243c4fe",
"assets/assets/images/me/center_set.png": "a993bc972a5791a78a30c704334bd51d",
"assets/assets/images/me/vip_level_bg.png": "c5c637b7eedc4a7383295deafef146fa",
"assets/assets/images/me/ic_me_set.png": "45a9b3670b988d636f59925d7bdecdaf",
"assets/assets/images/me/img_me_vip_select.png": "f1110fddd770047af56889fe80d8a721",
"assets/assets/images/me/vip_permant_bg_select.png": "2d79cbcd7da486b182f060b42c4e9c5f",
"assets/assets/images/me/edit_sel1.png": "2eb954b3fb72975b8cdf3818ad1483f6",
"assets/assets/images/me/3.0x/me_banner_hot_left.png": "4ba14ef3566e7d91413771a4eb648f3b",
"assets/assets/images/me/3.0x/edit_nor.png": "8bac30d8af230e1ab0be67b7637a62b9",
"assets/assets/images/me/3.0x/ic_me_vip_year.png": "2db138bb7c15f5e36b76c58f4721efca",
"assets/assets/images/me/3.0x/ic_me_goldcoin.png": "b276587fb40ef897c03d4d028cba704c",
"assets/assets/images/me/3.0x/img_me_vip_crown.png": "d95a241b922bb4f8d249aa98aa9f701b",
"assets/assets/images/me/3.0x/img_me_coupon_btn.png": "0f514c219e23488307ae19bfa2c0fc59",
"assets/assets/images/me/3.0x/img_wallet_titlebg.png": "2d44f9bc7b209a0392c034df8f4f3bf5",
"assets/assets/images/me/3.0x/img_me_up.png": "717180cfe42aabe4beac42b595892605",
"assets/assets/images/me/3.0x/me_vip_card.png": "f1f93626e1ef262e6c5782c1b3f16ff1",
"assets/assets/images/me/3.0x/vip_tips.png": "6e7e4be6721a49fbb8854084fc65e765",
"assets/assets/images/me/3.0x/me_vip_bg.png": "cde95eb78eb22aec8cebbfd88110f859",
"assets/assets/images/me/3.0x/vip_member_never.png": "6ec263ee977a8fb0747b2cf95ffbb19f",
"assets/assets/images/me/3.0x/center_set.png": "a806acd854b54f2ce6d1a739ff63c03f",
"assets/assets/images/me/3.0x/vip_level_bg.png": "f7f8bf82f6b8d446967650ec4faba4a3",
"assets/assets/images/me/3.0x/ic_me_set.png": "a5cf165bf6f73f0c81ef599eb24fc2e4",
"assets/assets/images/me/3.0x/img_me_vip_select.png": "c1ab79915fecf5181051b8e52904d60e",
"assets/assets/images/me/3.0x/vip_permant_bg_select.png": "b450e19d6944034a601e800077292680",
"assets/assets/images/me/3.0x/edit_sel1.png": "6d1c0386f0353d4c9b0d40c923bd96b9",
"assets/assets/images/me/3.0x/icon_server.png": "f9a8d0e6d804f730dc334606fb020f22",
"assets/assets/images/me/3.0x/vip_top_bg.png": "856928a972b6a26855748ce2573332ef",
"assets/assets/images/me/3.0x/img_me_vip_btn.png": "5ca3ad365ca7d8e7cd0b4d39ab4ee38d",
"assets/assets/images/me/3.0x/ic_more.png": "8b18a4cb9b10cedb933ee6fccc01f41e",
"assets/assets/images/me/3.0x/ic_me_edit.png": "c68faa2ee0a9acc473a4bfb5288a6a49",
"assets/assets/images/me/3.0x/vip_member_3.png": "66759a6a738242a762315a3ef00cfdb4",
"assets/assets/images/me/3.0x/img_me_wallet_titlebg.png": "5ccddc952b3fd87b86d36937285fe91a",
"assets/assets/images/me/3.0x/ic_me_vipno.png": "e8cfab78f69d1ba3fbde6ccb8ff9af49",
"assets/assets/images/me/3.0x/vip_member_1.png": "43b9be78dbdae1ec2c14d2feba4f6d42",
"assets/assets/images/me/3.0x/vip_bandon_top.png": "cf2c399c910fd005ad3fed245d59ae2c",
"assets/assets/images/me/3.0x/me_banner_coupon_left.png": "adc0350660f770dade1e91e8684ab08a",
"assets/assets/images/me/3.0x/me_vip_level.png": "e0dba3e9fccea57d7f53488068511398",
"assets/assets/images/me/3.0x/me_banner_coupon_title.png": "e4e28e5cf7ad2f165197505c9b3069f2",
"assets/assets/images/me/3.0x/vip_rim.png": "d95a241b922bb4f8d249aa98aa9f701b",
"assets/assets/images/me/3.0x/img_me_vip_choose.png": "3b985a9583efcb5f1241bebd353d43f5",
"assets/assets/images/me/3.0x/vip_year_select.png": "5f86f0912132ab84339c0d761b80bbc9",
"assets/assets/images/me/3.0x/ic_question.png": "abc3e7b88689e1b13d70566d00755244",
"assets/assets/images/me/3.0x/vip_Fmvo.png": "4cbd91bc560309e3b1522f69d85f35c9",
"assets/assets/images/me/3.0x/me_banner_hot_bg.png": "596441851399c828e2c20f8ce9f64506",
"assets/assets/images/me/3.0x/ic_me_vip_quarter.png": "e0dba3e9fccea57d7f53488068511398",
"assets/assets/images/me/3.0x/ic_me_vip_forever.png": "62410275d2ec37979dd5bce90a942564",
"assets/assets/images/me/3.0x/img_me_wallet_title.png": "ae0c8a7f8d69d311fa368bfb4925c9f6",
"assets/assets/images/me/3.0x/img_me_topbg.png": "764bb675489d6a5bcb4eda9b61f5aa16",
"assets/assets/images/me/3.0x/uper_legalize.png": "bc8d729161c7eb5cc230ad2e19c58dd6",
"assets/assets/images/me/3.0x/ic_me_vip_month.png": "d2c6cca8b72a8bfb71662954b836ffb2",
"assets/assets/images/me/3.0x/btn_close.png": "fbc6b6b5d193d7b77a01a3415a8bdc81",
"assets/assets/images/me/3.0x/img_me_vip_title.png": "e63965ba7e159b2ddf3ab3316beb93df",
"assets/assets/images/me/3.0x/vip_member_365.png": "715107d001873b47fc8d56c0c81a0b39",
"assets/assets/images/me/3.0x/icon_vip_item_tips.png": "4c8838564a22f0eec8bb72ab344c30be",
"assets/assets/images/me/3.0x/me_banner_hot_title.png": "e37322bd4522ab7c56be9b463f08c097",
"assets/assets/images/me/3.0x/me_banner_coupon_bg.png": "da77bc62d0bac4b5aa99fce6905ca86e",
"assets/assets/images/me/3.0x/uper_tips.png": "e1fbea625f542aa900ebf5bea5ffd2bc",
"assets/assets/images/me/3.0x/img_me_wallet_btn.png": "d08619b56fdbd1bc0d63e6436508ca77",
"assets/assets/images/me/icon_server.png": "ad9db884674f0b80175ed09816478f3e",
"assets/assets/images/me/vip_top_bg.png": "d1365bf4b60692375335668a24a071b2",
"assets/assets/images/me/img_me_vip_btn.png": "65008ec64e20f78f79179f594b80362b",
"assets/assets/images/me/ic_more.png": "ba6003a1314dddbc2cc19a293517e6f1",
"assets/assets/images/me/ic_me_edit.png": "ec143901610fc8ea87205d117c5b60eb",
"assets/assets/images/me/vip_member_3.png": "60945cd225d24cb614ca65541ad4431f",
"assets/assets/images/me/img_me_wallet_titlebg.png": "82a4686b5dd7b2d6add28bfca5ea6312",
"assets/assets/images/me/ic_me_vipno.png": "a6519c33808be0c7d5e501d36dd151c9",
"assets/assets/images/me/vip_member_1.png": "930527ae3bcea86ca02d911cd9378c9f",
"assets/assets/images/me/vip_bandon_top.png": "90cafba7ddd98c64af89bf514af291c2",
"assets/assets/images/me/me_banner_coupon_left.png": "9b012e8ab44eb32ad35c3b02f337e16e",
"assets/assets/images/me/me_vip_level.png": "d3e2c98d12b06268ec83e6837c7de86c",
"assets/assets/images/me/me_banner_coupon_title.png": "75ae64273b9b5986ea754b130e35cb69",
"assets/assets/images/me/vip_rim.png": "4bcfe7a976e81bdcb57e07d5351ca8e9",
"assets/assets/images/me/img_me_vip_choose.png": "0043da8738b8fa522e15afab760dbaec",
"assets/assets/images/me/vip_year_select.png": "39314ab805de1271473015e80e235fc1",
"assets/assets/images/me/ic_question.png": "abc3e7b88689e1b13d70566d00755244",
"assets/assets/images/me/vip_Fmvo.png": "a7f4184bce4ebfe3e2561467082d3b8b",
"assets/assets/images/me/me_banner_hot_bg.png": "fa9444e364c59978de90e838a30f1474",
"assets/assets/images/me/ic_me_vip_quarter.png": "100d94af15f81383229e72d696f451b3",
"assets/assets/images/me/ic_me_vip_forever.png": "bebf7eb64ca70b27f9e786108e6fee98",
"assets/assets/images/me/f2d_logo.png": "a0ad54a0bcd3d1566267ee63d1ec2a1d",
"assets/assets/images/me/img_me_wallet_title.png": "bb0bacf8d8cd95efe775ad5ba26f6f9c",
"assets/assets/images/me/img_me_topbg.png": "8b51be61ab1df6f221af26462d6864b5",
"assets/assets/images/me/uper_legalize.png": "a3619ae35e501778823e4e26e75598e2",
"assets/assets/images/me/ic_me_vip_month.png": "6ced54d8e308041b826986359b525d5f",
"assets/assets/images/me/btn_close.png": "2f283b103f62cad8339d8fde8e484758",
"assets/assets/images/me/img_me_vip_title.png": "c71969e68f52e4598c102b762e722a15",
"assets/assets/images/me/vip_member_365.png": "f73d66a448e5bdf4230598f464aa5b3d",
"assets/assets/images/me/icon_vip_item_tips.png": "f9fc6791061c57739e4dd390d2deabab",
"assets/assets/images/me/me_banner_hot_title.png": "5a93eb7b8272dd4b3427337280d413ef",
"assets/assets/images/me/me_banner_coupon_bg.png": "4fb69f4afd3629d456eb1fc7b7e8fd04",
"assets/assets/images/me/uper_tips.png": "7fb8b7a77f60be100c1bff05a83d53a6",
"assets/assets/images/me/img_me_wallet_btn.png": "6f32db1e13d6f5f10f800492a8f22dd7",
"assets/assets/images/hot/ic_rank_loufeng.png": "4e3b938600aee1f396860f496cd69757",
"assets/assets/images/hot/ic_rank_topic.png": "dc991d5ddab9577fe8fee2a2fc033c09",
"assets/assets/images/hot/ic_rank_hotvideo.png": "e350aaf030b5e91f9acd8b238161618d",
"assets/assets/images/hot/hot_vip_icon.png": "2fc18cc56ff8bd8fd51deee354216c93",
"assets/assets/images/hot/ic_rank_coin.png": "98d6a690534401008ef304c72b2f1d9c",
"assets/assets/images/hot/ic_rank_vip.png": "918ebfd402e4eeaf0577b33e1c070ebd",
"assets/assets/images/hot/img_rank_loufeng_ad.png": "7661856a8d099acad90a3356ce6208bb",
"canvaskit/canvaskit.js": "62b9906717d7215a6ff4cc24efbd1b5c",
"canvaskit/profiling/canvaskit.js": "3783918f48ef691e230156c251169480",
"canvaskit/profiling/canvaskit.wasm": "6d1b0fc1ec88c3110db88caa3393c580",
"canvaskit/canvaskit.wasm": "b179ba02b7a9f61ebc108f82c5a1ecdb"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
